import math, os, stat
from psutil import virtual_memory
from charms.reactive import when, when_not, when_any, is_flag_set, set_flag, clear_flag, endpoint_from_flag
from charms.templating.jinja2 import render
from charmhelpers.core.hookenv import open_port, resource_get, status_set, log
from subprocess import check_call


@when_not('druid-coordinator.installed')
def install_druid_coordinator():
    if check_system_memory() < 3:
        log('Cannot start Coordinator, system has ' + str(check_system_memory()) + 'GB of RAM but Coordinator requires at least 3GB of RAM')
        status_set('blocked', 'Coordinator requires at least 3GB of RAM')
    else:
        archive = resource_get("druid")
        if not os.path.isdir('/opt/druid_coordinator'):
            os.mkdir('/opt/druid_coordinator')
        cmd = ['tar', 'xfz', archive, '-C', '/opt/druid_coordinator', '--strip', '1']
        check_call(cmd)

        archive = resource_get("mysql-extension")
        cmd = ['tar', 'xfz', archive, '-C', '/opt/druid_coordinator/extensions']
        check_call(cmd)

        render('druid_coordinator', '/etc/init.d/druid_coordinator')
        render('druid_logrotate', '/etc/logrotate.d/druid_logrotate')
        st = os.stat('/etc/init.d/druid_coordinator')
        os.chmod('/etc/init.d/druid_coordinator', st.st_mode | stat.S_IEXEC)

        open_port(8081)

        set_flag('druid-coordinator.installed')
        status_set('waiting', 'Waiting for config file')


@when('druid-coordinator.installed', 'endpoint.config.new_config')
def configure_druid_coordinator():
    with open('/opt/druid_coordinator/conf/druid/_common/common.runtime.properties', 'r') as c:
        old_conf = c.read()

    config = endpoint_from_flag('endpoint.config.new_config')
    new_conf = config.get_config()

    if old_conf != new_conf:
        log('New config detected! Resetting Druid Coordinator.')
        if is_flag_set('druid-coordinator.configured'):
            clear_flag('druid-coordinator.configured')

        status_set('maintenance', 'Configuring Coordinator')

        config_file = open('/opt/druid_coordinator/conf/druid/_common/common.runtime.properties', 'w')
        config_file.write(new_conf)
        config_file.close()

        set_flag('druid-coordinator.configured')
        set_flag('druid-coordinator.new_config')
        status_set('maintenance', 'Druid Coordinator configured, waiting to start process...')


@when('druid-coordinator.installed', 'endpoint.config.new_hdfs_files')
@when_not('druid-coordinator.hdfs_configured')
def configure_hdfs_files():
    hdfs = endpoint_from_flag('endpoint.config.new_hdfs_files')
    new_hdfs_files = hdfs.get_hadoop_files()

    status_set('maintenance', 'Copying HDFS XML Files.')

    with open('/opt/druid_coordinator/conf/druid/_common/core-site.xml', 'w') as f:
        f.write(new_hdfs_files[0])
    with open('/opt/druid_coordinator/conf/druid/_common/hdfs-site.xml', 'w') as f:
        f.write(new_hdfs_files[1])
    with open('/opt/druid_coordinator/conf/druid/_common/mapred-site.xml', 'w') as f:
        f.write(new_hdfs_files[2])
    with open('/opt/druid_coordinator/conf/druid/_common/yarn-site.xml', 'w') as f:
        f.write(new_hdfs_files[3])

    if is_flag_set('druid-coordinator.configured'):
        clear_flag('druid-coordinator.configured')

    set_flag('druid-coordinator.hdfs_configured')
    set_flag('druid-coordinator.new_config')
    status_set('waiting', 'HDFS files copied. Waiting...')

@when_any('druid-coordinator.configured', 'druid-coordinator.hdfs_configured')
@when('java.ready', 'druid-coordinator.new_config')
def run_druid_coordinator(java):
    restart_coordinator()
    clear_flag('druid-coordinator.new_config')

def start_coordinator():
    status_set('maintenance', 'Starting Coordinator...')
    cmd = ['/etc/init.d/druid_coordinator', 'start']
    check_call(cmd)
    set_flag('druid-coordinator.running')
    status_set('active', 'Coordinator running')

def stop_coordinator():
    cmd = ['/etc/init.d/druid_coordinator', 'stop']
    check_call(cmd)
    clear_flag('druid-coordinator.running')

def restart_coordinator():
    status_set('maintenance', 'Restarting Coordinator...')
    stop_coordinator()
    start_coordinator()

def write_jvm_config():
    mem = virtual_memory()
    mem_gb = mem.total / (1024**3)

    Xmx = math.floor(mem_gb)

    context = {
        'Xmx': Xmx
    }

    render('jvm.config', '/opt/druid_coordinator/conf/druid/coordinator/jvm.config', context)



def check_system_memory():
    """
    Returns the amount of system memory of the system, truncated to only full gigabytes.
    """
    mem = virtual_memory()
    return mem.total // (1024**3)
