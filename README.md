# Overview

Welcome to the Druid Coordinator charm by Spicule. The Druid coordinator node is 
primarily responsible for segment management and distribution. More 
specifically, the Druid coordinator node communicates to historical nodes to 
load or drop segments based on configurations. The Druid coordinator is 
responsible for loading new segments, dropping outdated segments, managing 
segment replication, and balancing segment load. Druid coordinator also exposes 
a web GUI for displaying cluster information and rule configuration.

For more information on Druid Coordinator, please visit the [Druid Coordinator Documentation](http://druid.io/docs/latest/design/coordinator.html).

# Usage

To deploy this charm from the command line, enter the following:

    juju deploy cs:~spiculecharms/druid-coordinator --series xenial

You can specify hardware constraints for your Coordinator node on the command 
line. To find out more, refer to the ["Using Constraints"](https://docs.jujucharms.com/2.4/en/charms-constraints").
For example, to deploy your Coordinator node on a server with 4 cores and 15 GB
of RAM, enter the following command:

    juju deploy cs:~spiculecharms/druid-coordinator --series xenial --constraints "mem=15G cores=4"

# OpenJDK and Druid Config

Druid Coordinator requires a relation to the [OpenJDK charm](https://jujucharms.com/openjdk/)
to automate the installation of Java onto Coordinator's server, which is a
requirement of Coordinator.

Additionally, Coordinator requires configuration as part of the wider Druid
cluster, so Coordinator must be related to our [Druid Config charm](https://jujucharms.com/new/u/spiculecharms/druid-config).
The individual Druid nodes do not need to be directly related to one another,
and should instead be directly related to Druid Config instead. 

# Exposing and Accessing the Console

Once Coordinator has been installed and configured correctly, you can expose the
Coordinator node through Juju, which will enable you to access the web console.
To expose Coordinator, enter the following command via the command line:

    juju expose druid-coordinator

This will open the port for your Coordinator node. Use the following command to
check the status of the server adjusting its firewall settings, as well as
getting the IP address of the server:

    juju status

Once the firewall has opened up port 8081 and you know your Coordinator node's IP
address, you can use your favourite web browser to visit the web console:

    http://<coordinator-ip-address>:8081

To unexpose your Coordinator instance, simply enter the following command through
the command line:

    juju unexpose druid-coordinator

# Configuration

Druid Cluster configuration is handled through the [Druid Config charm](https://jujucharms.com/new/u/spiculecharms/druid-config).

# Contact Information

Thank you for using the Druid Coordinator charm - we hope you find it useful! For
more information, here are our contact details:

Spicule: [http://www.spicule.co.uk](http://www.spicule.co.uk)  
Anssr: [http://anssr.io](http://anssr.io)  
Email: info@spicule.co.uk  

For assistance with Juju, please refer to the [Juju Discourse](https://discourse.jujucharms.com/).

## Druid

For more information about Druid, please refer to the [Druid website](http://druid.io).

